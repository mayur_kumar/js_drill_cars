export const lastCarDetails = function (inventory){
    let length = inventory.length
    let car = inventory[length-1]
    return (`Last car is a ${car["car_make"]} ${car['car_model']}`)
} 