// The marketing team wants the car models listed 
// alphabetically on the website. Execute a function to 
// Sort all the car model names into alphabetical order and log
//  the results in the console as it was returned.

export const sortModels = function (inventory) {

    let models = []

    for (let index =0 ;index<inventory.length;index++){
        let car = inventory[index]
        models.push(car["car_model"])
    }

    models.sort()

    return models
}