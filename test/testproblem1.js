import {findCarOfId} from "../problem1.js"
import {inventory} from "./inventory.js"

let car = findCarOfId(inventory,33)
console.log(`Car ${car["id"]} is a ${car["car_year"]} ${car["car_make"]} ${car["car_model"]}`)